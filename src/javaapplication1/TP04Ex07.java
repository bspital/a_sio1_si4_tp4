package javaapplication1;

public class TP04Ex07 {

    public static void main(String[] args) {
           
       String aClair = " ABCDEFGHIJKLMNOPQRSTUVWXYZ";
       String aCode  = "JYULFZCENGPD AWBHOKXQISVRMT";
       
       String phraseEnCode  = "LZ YJCYGQJXZHQJXZAYGWZXJOIZJWBIXJHKBEKYAABWXJZWJPYSY";
       for(int i=0; i<=phraseEnCode.length()-1; i++) {
           char letterEnCode = phraseEnCode.charAt(i);
           int placeLetterEnCodeInCode = aCode.indexOf(letterEnCode);
           char letterClair = aClair.charAt(placeLetterEnCodeInCode);
           System.out.printf(Character.toString(letterClair));
       }
       System.out.println("\n");
   }
         
} 