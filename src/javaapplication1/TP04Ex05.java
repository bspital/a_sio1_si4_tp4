package javaapplication1;
public class TP04Ex05 {

    public static void main(String[] args) {
        
         int[]   tableau = { 33, 56, 14, 76, 81, 2, 57, 13, 61, 64, 45, 35, 95, 15 };
         float moyImpair = 0, moyPair = 0, nbPair = 0, nbImpair = 0;
         
         for(int i=0; i<=tableau.length-1; i++) {
             if(tableau[i]%2==1) {
                 moyImpair = moyImpair + tableau[i];
                 nbImpair++;
             }
             if(tableau[i]%2==0) {
                 moyPair = moyPair + tableau[i];
                 nbPair++;
             }
         }
         System.out.println("La moyenne des nombres pairs est de "+moyPair/nbPair);
         System.out.println("La moyenne des nombres paires est de "+moyImpair/nbImpair);
          
    } 
}

 
