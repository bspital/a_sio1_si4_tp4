package javaapplication1;

import java.util.Arrays;
import java.util.List;

public class TP04Ex08 {
    
    public static void main(String[] args) {
       
        String          alphaClair = " ABCDEFGHIJKLMNOPQRSTUVWXYZO123456789";
        
        List<String>    alphaMorse= Arrays.asList
                                    
                                    (
                                      "  ",
                                      ".-","-...","-.-.","-..",".","..-.","--.","....","..",".---",
                                      "-.-",".-..","--","-.","---","-.","--.-",".-.","...","-",
                                      "..-","...-",".--","-..-","-.--","--..",
                                      ".----","..---","...--","....-",".....",
                                      "-....","--...","---..","---.","-----"
                                    );
                
         String messDetresse="SOS TITANIC SINKING BY THE HEAD WE ARE ABOUT ALL DOWN";
                 
       for(int i=0; i<=messDetresse.length()-1; i++) {
           char letter = messDetresse.charAt(i);
           int emplacementLettre = alphaClair.indexOf(letter);
           System.out.printf(alphaMorse.get(emplacementLettre));
       }
        System.out.println("\n");
         
    }
}
