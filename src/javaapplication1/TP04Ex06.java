package javaapplication1;
public class TP04Ex06 {

    public static void main(String[] args) {
        
         String[]   fruits = {"pomme",     "poire",     "abricot", "ananas",    "citron",
                              "péche",     "mirabelle", "fraise",  "framboise", "raisin",
                              "groseille", "prune",     "orange",  "banane" };
         for(int i=0; i<=fruits.length-1; i++) {
             if(fruits[i].length() == 6) {
                 System.out.println(fruits[i]);
             }
         }
    }
}