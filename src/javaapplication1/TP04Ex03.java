package javaapplication1;
public class TP04Ex03 {

    public static void main(String[] args) {
        
          float[] notes = { 12, 8, 14, 7, 9.5f, 14, 15.5f, 6, 16, 11, 14, 10.5f };
          float moyenne = 0, min = 20, max = 0;
          
          for(int i=0; i <= notes.length-1; i++) {
              moyenne = moyenne+notes[i];
              if(notes[i]>max) max=notes[i];
              if(notes[i]<min) min=notes[i];
          }
          System.out.println("La moyenne des notes est de: "+moyenne / notes.length);
          System.out.println("La note maximale est de "+max);
          System.out.println("La note minimale est de "+min);
    }
}
